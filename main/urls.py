from django.urls import path 

from . import views

urlpatterns = [
    path("<int:id>", views.index, name="index"),
    path("", views.home, name="home"), 
    path("start/", views.index, name="index"),
    path("movie/", views.movie, name="movie"),
    path('user_list/', views.user_list, name='user_list'),
    path('book_list/', views.book_list, name='book_list'),
    path('add/', views.add_task, name='add_task'),
    path('list/', views.task_list, name='task_list'),
    path('login/', views.login, name='login'),
]

# start 
