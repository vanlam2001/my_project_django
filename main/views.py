from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import ToDoList, Item, User, Book, Task, Login_Page



# Create your views here.
def index(response, id):
    ls = ToDoList.objects.get(id=id)
    return render(response, "main/list.html", {"ls": ls})

def home(response):
    return render(response, "main/home.html", {})


def movie(response):
    return HttpResponse("<h2>Movie Page <h2>")

def user_list(request):
    users = User.objects.all()
    return render(request, 'main/user_list.html', {'users': users})

def book_list(request):
    books = Book.objects.all()
    return render(request, 'main/book_list.html', {'books': books})

def add_task(request):
    if request.method == 'POST':
        title = request.POST['title']
        description = request.POST['description']
        task = Task.objects.create(title=title, description=description)
        return redirect('task_list')  # Điều hướng đến trang hiển thị danh sách tasks

    return render(request, 'main/add_task.html')

def task_list(request):
    tasks = Task.objects.all()
    return render(request, 'main/task_list.html', {'tasks': tasks})

def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        try:
            user = Login_Page.objects.get(username=username, password=password)
            # Thực hiện xử lý đăng nhập tại đây
            # Ví dụ: lưu thông tin đăng nhập vào session
            request.session['user_id'] = user.id
            return redirect('home')
        except Login_Page.DoesNotExist:
            return render(request, 'main/login.html', {'error': 'Tên đăng nhập hoặc mật khẩu không đúng.'})

    return render(request, 'main/login.html')

